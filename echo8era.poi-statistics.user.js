// ==UserScript==
// @name         poi-statistics Translation Userscript
// @namespace    uri:echo8era/poi-statistics
// @version      0.2.0.18
// @description  Translate poi-statistics pages
// @author       Echo 8 (https://forums.spacebattles.com/members/echo-8.30201/)
// @match        https://db.kcwiki.org/*
// @resource     resources     https://gitlab.com/Echo-8-ERA/poi-statistics.Translation/raw/master/echo8era.poi-statistics.resources.json
// @resource     page-mods     https://gitlab.com/Echo-8-ERA/poi-statistics.Translation/raw/master/echo8era.poi-statistics.page-mods.json
// @resource     stylesheet    https://gitlab.com/Echo-8-ERA/poi-statistics.Translation/raw/master/echo8era.poi-statistics.stylesheet.css
// @resource     item-trans    https://raw.githubusercontent.com/KC3Kai/kc3-translations/master/data/en/items.json
// @resource     ship-trans    https://raw.githubusercontent.com/KC3Kai/kc3-translations/master/data/en/ships.json
// @resource     other-trans   https://gitlab.com/Echo-8-ERA/poi-statistics.Translation/raw/master/echo8era.poi-statistics.translations.json
// @grant        GM_getResourceText
// @grant        GM_addStyle
// ==/UserScript==

(function() {
		'use strict';

		const pageMods = JSON.parse(GM_getResourceText("page-mods"));
		const resources = JSON.parse(GM_getResourceText("resources"));
		const translations = { };
		[ "item-trans", "ship-trans", "other-trans" ].forEach(name => {
				let dict = JSON.parse(GM_getResourceText(name));
				translations[name] = {
							"regexp": RegExp("^(?:" + Object.keys(dict).map(key => key.replace(/[-[\]{}()*+!<=:?.\/\\^$|#\s,]/g, '\\$&')).join("|") + ")$"),
							"map": dict
					};
			});

		const WaitFor = function(conditionCallback, callback) {

			if (conditionCallback())
				callback();
			else
				setTimeout(() => WaitFor(conditionCallback, callback), 1000);

		};

		const processData = function(data) {
				switch (data.type) {
					case "text":
						return data.value;
					case "resource":
						let item = resources;
						for (let index of data.path) {
							item = item[index];
						}
						return item;
					case "element":
						let elem = document.createElement(data.tag);
						if (data.attributes) {
							for (let attr in data.attributes) {
								elem.setAttribute(attr, processData(data.attributes[attr]));
							}
						}
						if (data.content) {
							for (let i = 0; i < data.content.length; ++i) {
								let child = processData(data.content[i]);
								if (typeof(child) === "string") {
									child = document.createTextNode(child);
								}
								elem.appendChild(child);
							}
						}
						return elem;
				}
			};

		GM_addStyle(GM_getResourceText("stylesheet"));

		WaitFor(() => document.querySelector("div.busy-indicator") ? document.querySelector("div.busy-indicator").style.display === "none" : true, () => {
				pageMods.forEach(pageMod => {
						if (pageMod.patterns.some(pattern => RegExp(pattern).test(document.URL))) {
							pageMod.data.forEach(item => {
									Array.prototype.forEach.call(document.querySelectorAll(item.selectors), elem => {
											let refElem = item.behaviour.endsWith("-element") ? elem.parentElement : elem;
											let markedForDeletion = [];
											let insertRef = null;

											switch (item.behaviour) {
												case "replace-content":
												case "delete-content":
													if (!item.range) item.range = [0, -1];
													let startIdx = item.range[0] >= 0 ? item.range[0] : elem.childNodes.length + item.range[0];
													let endIdx = item.range[1] >= 0 ? item.range[1] : elem.childNodes.length + item.range[1]
													for (let i = startIdx; i <= endIdx; ++i)
														markedForDeletion.push(elem.childNodes[i]);
													insertRef = markedForDeletion[markedForDeletion.length - 1].nextSibling;
													break;

												case "replace-element":
												case "delete-element":
													if (!item.range) item.range = [0, 0];
													for (let i = item.range[0] - 1, curNode = elem; i > 0; --i, curNode = curNode.previousSibling)
														markedForDeletion.push(curNode);
													for (let i = 0, curNode = elem; i <= item.range[1]; ++i, curNode = curNode.nextSibling)
														markedForDeletion.push(curNode);
													insertRef = markedForDeletion[markedForDeletion.length - 1].nextSibling;
													break;

												case "insert-element":
													insertRef = elem;
													break;

												case "append-element":
													insertRef = elem.nextSibling;
													break;

												case "insert-content":
													insertRef = elem.childNodes[item.index ? (item.index > 0 ? item.index : elem.childNodes.length + item.index) : 0];
													break;

												case "append-content":
													if (item.index)
														insertRef = elem.childNodes[item.index > 0 ? item.index : elem.childNodes.length + item.index].nextSibling;
													break;

												case "apply-dictionary":
													Array.prototype.forEach.call(elem.childNodes, node => {
															if (node.nodeType === Node.TEXT_NODE && node.nodeValue && node.nodeValue.length > 0)
																item.content.forEach(dict => node.nodeValue = node.nodeValue.replace(translations[dict].regexp, match => translations[dict].map[match]));
														});
													return;
											}

											if (markedForDeletion.length > 0) {
												insertRef = markedForDeletion[markedForDeletion.length - 1].nextSibling;
												markedForDeletion.forEach(e => refElem.removeChild(e));
											}

											if (/^(append|insert|replace)-/.test(item.behaviour)) {
												item.content.forEach(content => {
														let newContent = processData(content);
														if (typeof(newContent ) === "string") newContent = document.createTextNode(newContent);
														refElem.insertBefore(newContent, insertRef);
													});
											}
										});
								});
						}
					});
				Array.prototype.forEach.call(document.querySelectorAll("*"), elem => {
						Array.prototype.forEach.call(elem.childNodes, node => {
								if (node.nodeType === Node.TEXT_NODE && node.nodeValue && node.nodeValue.length > 0)
									Object.values(translations).forEach(translation => node.nodeValue = node.nodeValue.replace(translation.regexp, match => translation.map[match]));
							});
					});
			});

})();